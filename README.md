% AMR (for R)

# `AMR` (for R)
<img src="man/figures/logo.png" align="right" height="120px" />

### Not a developer? Then please visit our website [https://msberends.gitlab.io/AMR](https://msberends.gitlab.io/AMR) to read about this package.

**It contains documentation about all of the included functions and also a comprehensive tutorial about how to conduct AMR analysis.**

## Development source

*NOTE: the original source code is on GitLab (https://gitlab.com/msberends/AMR). There is a mirror repository on GitHub (https://github.com/msberends/AMR). As the mirror process is automated by GitLab, both repositories always contain the latest changes.*

This is the **development source** of `AMR`, a free and open-source [R package](https://www.r-project.org) to simplify the analysis and prediction of Antimicrobial Resistance (AMR) and to work with microbial and antimicrobial properties by using evidence-based methods.

## Authors
Matthijs S. Berends<sup>1,2</sup>,
Christian F. Luz<sup>1</sup>,
Alex W. Friedrich1</sup>,
Bhanu N.M. Sinha<sup>1</sup>,
Casper J. Albers<sup>3</sup>,
Corinna Glasner<sup>1</sup>
  
<sup>1</sup> Department of Medical Microbiology, University of Groningen, University Medical Center Groningen, Groningen, the Netherlands - [rug.nl](http://www.rug.nl) [umcg.nl](http://www.umcg.nl)<br>
<sup>2</sup> Certe Medical Diagnostics & Advice, Groningen, the Netherlands - [certe.nl](http://www.certe.nl)<br>
<sup>3</sup> Heymans Institute for Psychological Research, University of Groningen, Groningen, the Netherlands - [rug.nl](http://www.rug.nl)<br>

<a href="https://www.rug.nl"><img src="man/figures/logo_rug.png" height="60px"></a>
<a href="https://www.umcg.nl"><img src="man/figures/logo_umcg.png" height="60px"></a>
<a href="https://www.certe.nl"><img src="man/figures/logo_certe.png" height="60px"></a>
<a href="http://www.eurhealth-1health.eu"><img src="man/figures/logo_eh1h.png" height="60px"></a>
<a href="http://www.eurhealth-1health.eu"><img src="man/figures/logo_interreg.png" height="60px"></a>

## How to get this package
Please see [our website](https://msberends.gitlab.io/AMR/#get-this-package).

## Copyright

This R package is licensed under the [GNU General Public License (GPL) v2.0](https://gitlab.com/msberends/AMR/blob/master/LICENSE). In a nutshell, this means that this package:

- May be used for commercial purposes

- May be used for private purposes

- May **not** be used for patent purposes

- May be modified, although:

  - Modifications **must** be released under the same license when distributing the package
  - Changes made to the code **must** be documented

- May be distributed, although:

  - Source code **must** be made available when the package is distributed
  - A copy of the license and copyright notice **must** be included with the package.

- Comes with a LIMITATION of liability

- Comes with NO warranty
