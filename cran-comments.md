* Although the previous release (1.0.1) is from 2020-02-23, this updates provides support for the upcoming dplyr 1.0.0 which is used (and relied upon) by probably all our users.

* For this specific version, otherwise nothing to mention.

* Since version 0.3.0, CHECK returns a NOTE for having a data directory over 3 MB. This is needed to offer users reference data for the complete taxonomy of microorganisms - one of the most important features of this package.
