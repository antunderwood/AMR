# ==================================================================== #
# TITLE                                                                #
# Antimicrobial Resistance (AMR) Analysis                              #
#                                                                      #
# SOURCE                                                               #
# https://gitlab.com/msberends/AMR                                     #
#                                                                      #
# LICENCE                                                              #
# (c) 2018-2020 Berends MS, Luz CF et al.                              #
#                                                                      #
# This R package is free software; you can freely use and distribute   #
# it for both personal and commercial purposes under the terms of the  #
# GNU General Public License version 2.0 (GNU GPL-2), as published by  #
# the Free Software Foundation.                                        #
#                                                                      #
# We created this package for both routine data analysis and academic  #
# research and it was publicly released in the hope that it will be    #
# useful, but it comes WITHOUT ANY WARRANTY OR LIABILITY.              #
# Visit our website for more info: https://msberends.gitlab.io/AMR.    #
# ==================================================================== #

# to do a full CRAN check with R-Hub:
# chck <- rhub::check(devtools::build(), platform = c('debian-clang-devel', 'debian-gcc-devel', 'fedora-clang-devel', 'fedora-gcc-devel', 'windows-x86_64-devel', 'debian-gcc-patched', 'solaris-x86-patched', 'debian-gcc-release', 'windows-x86_64-release', 'macos-elcapitan-release', 'windows-x86_64-oldrel'))

stages:
  - check
  - lint
  - coverage
  - website
  
image: rocker/r-base

before_script:
  - apt-get update -qq  --allow-releaseinfo-change
  # install dependencies for packages
  - apt-get install -y wget locales libxml2-dev libssl-dev libcurl4-openssl-dev zlib1g-dev > /dev/null
  # recent pandoc
  - wget --quiet https://github.com/jgm/pandoc/releases/download/2.7.3/pandoc-2.7.3-1-amd64.deb
  - dpkg -i pandoc*.deb
  - rm pandoc*.deb
  # set R system language
  - echo "LC_ALL=en_US.UTF-8" >> /etc/environment
  - echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen
  - echo "LANG=en_US.UTF-8" > /etc/locale.conf
  - locale-gen
  # cache R packages
  - mkdir -p installed_deps
  - echo 'R_LIBS="installed_deps"' > .Renviron
  - echo 'R_LIBS_USER="installed_deps"' >> .Renviron
  - echo 'R_LIBS_SITE="installed_deps"' >> .Renviron
  # set language
  - echo 'LANGUAGE="en_US.utf8"' >> .Renviron
  - echo 'LANG="en_US.utf8"' >> .Renviron
  - echo 'LANGUAGE="en_US.utf8"' > ~/.Renviron

R-release:
  stage: check
  allow_failure: false
  script:
    - Rscript -e 'sessionInfo()'
    # install missing and outdated packages
    - Rscript -e 'source(".gitlab-ci.R"); gl_update_pkg_all(repos = "https://cran.rstudio.com", quiet = TRUE, install_pkgdown = TRUE,  install_lintr = TRUE)'
    # remove vignettes folder and get VignetteBuilder field out of DESCRIPTION file
    - rm -rf vignettes
    - Rscript -e 'd <- read.dcf("DESCRIPTION"); d[, colnames(d) == "VignetteBuilder"] <- NA; write.dcf(d, "DESCRIPTION")'
    # build package
    - R CMD build . --no-build-vignettes --no-manual
    - PKG_FILE_NAME=$(ls -1t *.tar.gz | head -n 1)
    - Rscript -e 'Sys.setenv(NOT_CRAN = "true")'
    - R CMD check "${PKG_FILE_NAME}" --no-build-vignettes --no-manual --as-cran
  artifacts:
    when: always
    paths:
      - '*.Rcheck/*'
    expire_in: '1 month'
  cache:
    key: r350
    paths:
      - installed_deps/

R-devel:
  stage: check
  image: rocker/r-devel
  allow_failure: true
  script:
    - Rscriptdevel -e 'sessionInfo()'
    # install missing and outdated packages
    - Rscriptdevel -e 'source(".gitlab-ci.R"); gl_update_pkg_all(repos = "https://cran.rstudio.com", quiet = TRUE)'
    # remove vignettes folder and get VignetteBuilder field out of DESCRIPTION file
    - rm -rf vignettes
    - Rscriptdevel -e 'd <- read.dcf("DESCRIPTION"); d[, colnames(d) == "VignetteBuilder"] <- NA; write.dcf(d, "DESCRIPTION")'
    # build package
    - Rdevel CMD build . --no-build-vignettes --no-manual
    - PKG_FILE_NAME=$(ls -1t *.tar.gz | head -n 1)
    - Rscript -e 'Sys.setenv(NOT_CRAN = "true")'
    - Rdevel CMD check "${PKG_FILE_NAME}" --no-build-vignettes --no-manual --as-cran
  artifacts:
    when: always
    paths:
      - '*.Rcheck/*'
    expire_in: '1 month'
  cache:
    key: r360
    paths:
      - installed_deps/

lintr:
  stage: lint
  allow_failure: true
  when: on_success
  cache:
    key: r350
    paths:
      - installed_deps/
    policy: pull # no uploading after run
  only:
    - premaster
    - master
  script:
    # check all syntax with lintr
    - Rscript -e 'lintr::lint_package()'

codecovr:
  stage: coverage
  allow_failure: true
  when: on_success
  cache:
    key: r350
    paths:
      - installed_deps/
    policy: pull # no uploading after run
  only:
    - premaster
    - master
  script:
    - apt-get install --yes git
    # codecov token is set in https://gitlab.com/msberends/AMR/settings/ci_cd
    # Sys.setenv(NOT_CRAN = 'true'), because otherwise skip_on_cran() will be applied on Covr too, resulting in extremely low coverage percentages
    - Rscript -e "Sys.setenv(NOT_CRAN = 'true'); cc <- covr::package_coverage(line_exclusions = list('R/atc_online.R', 'R/mo_history.R', 'R/mo_source.R', 'R/resistance_predict.R')); covr::codecov(coverage = cc, token = '$codecov'); cat('Code coverage:', covr::percent_coverage(cc))"
  coverage: '/Code coverage: \d+\.\d+/'

pages:
  stage: website
  when: always
  only:
    - master
  script:
    - mv docs public
  artifacts:
    paths:
      - public
